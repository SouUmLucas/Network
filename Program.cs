﻿using System;
using System.IO;
using System.Net;
using System.Text;
using static System.Console;

namespace Network
{
    class Program
    {
        static void Main(string[] args)
        {
            // Listen on port 51111, serving files in c:\webroot:
            var server = new WebServer("http://localhost:51111/", @"c:\dev\csharp\webroot");
            try
            {
                server.Start();
                WriteLine("Running server... press Enter key to stop.");
                ReadLine();
            }
            finally { server.Stop(); }
        }
    }
}
